# Test Cases

SUT: [https://www.swift.org](https://www.swift.org)

## Test Case 1: Homepage Title

| Step No | What done                                           | Status | Comment |
|---------|-----------------------------------------------------|--------|---------|
| 1       | Load the homepage at 'https://www.swift.org'       | ✅      |         |
| 2       | Check if the homepage title contains "Swift.org"   | ✅      |         |

This test case checks if the homepage title contains "Swift.org".

Verifying that the title contains "Swift.org" ensures that the website is correctly loaded, and the expected content is displayed.

## Test Case 2: Navigation Bar

| Step No | What done                                                 | Status | Comment |
|---------|-----------------------------------------------------------|--------|---------|
| 1       | Load the homepage at 'https://www.swift.org'             | ✅      |         |
| 2       | Get all navigation items using XPath                     | ✅      |         |
| 3       | Compare the navigation items' text and links to the expected values | ✅      |         |

This test case checks if the navigation bar contains the expected items and links.

Ensuring that the navigation bar contains the correct items and links is important for a smooth user experience. It also verifies that the website structure is as expected and no links are broken or missing.

## Test Case 3: Documentation Page Header

| Step No | What done                                                 | Status | Comment |
|---------|-----------------------------------------------------------|--------|---------|
| 1       | Load the homepage at 'https://www.swift.org'             | ✅      |         |
| 2       | Click the "Documentation" button in the navigation bar   | ✅      |         |
| 3       | Wait for the Documentation page to load                  | ✅      |         |
| 4       | Check if the "Swift Documentation" section header is present | ✅      |         |

This test case navigates to the Documentation page by clicking the "Documentation" button on the main page and checks if the "Swift Documentation" section header is present.

Verifying the presence of the "Swift Documentation" section header ensures that the page has loaded correctly and confirms that the "Documentation" button on the main page works as intended.
