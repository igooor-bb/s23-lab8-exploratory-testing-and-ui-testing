import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By

class SwiftOrgTests(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()

    def test_homepage_title(self):
        self.driver.get('https://www.swift.org')
        self.assertIn('Swift.org', self.driver.title)

    def test_navigation_bar(self):
        self.driver.get('https://www.swift.org')
        nav_items = self.driver.find_elements(By.XPATH, '/html/body/nav/div[2]//ul[1]//a')
        nav_items = set([(item.text.lower(), item.get_attribute('href')) for item in nav_items])
        
        expected_items = [
            ('about swift', 'https://www.swift.org/about/'),
            ('blog', 'https://www.swift.org/blog/'),
            ('getting started', 'https://www.swift.org/getting-started/'),
            ('download', 'https://www.swift.org/download/'),
            ('platform support', 'https://www.swift.org/platform-support/'),
            ('documentation', 'https://www.swift.org/documentation/')
        ]
        
        self.assertEqual(len(nav_items), len(expected_items))
        for item in expected_items:
            self.assertIn(item, nav_items)

    def test_documentation_page_header(self):
        self.driver.get('https://www.swift.org')
        documentation_button = self.driver.find_element(By.XPATH, '//nav//a[text()="Documentation"]')
        documentation_button.click()
        self.driver.implicitly_wait(3)
        section_header = self.driver.find_element(By.XPATH, '/html/body/main/article/header/h1')
        self.assertEqual(section_header.text, 'Documentation')

    def tearDown(self):
        self.driver.quit()

if __name__ == '__main__':
    unittest.main()
